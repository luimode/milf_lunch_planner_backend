const VoteOptionsTableName = process.env.VOTE_OPTIONS_TABLE_NAME || 'LunchOptionsTable';
const AWS = require('aws-sdk');
const {validate} = require('./schema');
const {readOptions} = require('./optionReader');
const docClient = new AWS.DynamoDB.DocumentClient();

const util = require('util');

const parseAndValidate = async body => {
    const jBody = JSON.parse(body);
    const errors = validate('vote-option-schema', jBody);
    if (errors.length > 0) {
        throw new Error(errors.join(','));
    }

    const isDuplicate = await optionExists(jBody.optionName);
    if (isDuplicate) {
        return Promise.reject("Duplicate option name:" + jBody.optionName);
    }

    return jBody;
};

const optionExists = async optionName => {
    try {
        const options = await readOptions();
        const matches = options.filter(option => option.optionName.toLowerCase() === optionName.toLowerCase());
        return matches.length > 0;
    } catch (ex) {
        console.log('optionExists::error' + ex);
        return Promise.reject(ex);
    }
};

const timeParams = () => {
    const ttlDate = new Date();
    ttlDate.setDate(ttlDate.getDate() + 1);
    ttlDate.setHours(0, 0, 0, 0);
    return {ttl: Math.floor(ttlDate.valueOf() / 1000)};
};

const registerOption = async (obj, dyn = docClient) => {
    try {
        const parsedBody = await parseAndValidate(obj);
        if (parsedBody) {
            const dbResult = await dyn.put({
                TableName: VoteOptionsTableName,
                Item: {
                    ...parsedBody,
                    ...timeParams()
                }})
                .promise();
            console.log(`registerOption::success! response: ${util.inspect(dbResult, false, null)}`);
            return 'Writing to DB done';
        }
    } catch (ex) {
        return Promise.reject(ex);
    }
};

module.exports = {
    registerOption,
};
