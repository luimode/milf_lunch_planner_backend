const VotesTableName = process.env.LUNCH_PLANNER_TABLE_NAME || 'LunchPlannerTable';
const VotersTableName = process.env.VOTERS_TABLE_NAME || 'LunchVotersTable';
const AWS = require('aws-sdk');
const {validate} = require('./schema');
const docClient = new AWS.DynamoDB.DocumentClient();

const util = require('util');

const findVoterName = async (voterId, dyn = docClient) => {
    try {
        const dbResult = await dyn.query({
            TableName: VotersTableName,
            KeyConditionExpression: 'voter_id=:vId',
            ExpressionAttributeValues: {':vId': voterId},
            ProjectionExpression: 'voter_id, #voterName, #created',
            ExpressionAttributeNames: {
                "#voterName": "name",
                "#created": "timestamp"
            }
        }).promise();

        if (dbResult.Count !== 1) {
            return Promise.reject("Unknown voterId:" + voterId);
        }

        return dbResult.Items[0].name;
    } catch (ex) {
        console.log(`addVoterName::error:voterId:${voterId}, ex:${ex}`);
    }
};

const enrichVote = async (parsedVote) => {
    const voterId = parsedVote.voterId;

    const voterName = await findVoterName(voterId);
    if (!voterName) {
        return;
    }
    delete parsedVote.voterId;
    parsedVote.voter_id = voterId;
    parsedVote['name'] = voterName;
    return parsedVote;
};

const parseAndValidate = async body => {
    const jBody = JSON.parse(body);
    const errors = validate('vote-schema', jBody);
    if (errors.length > 0) {
        throw new Error(errors.join(','));
    }

    return await enrichVote(jBody);
};

const timeParams = () => {
    const timestamp = Date.now();
    const ttlDate = new Date();
    ttlDate.setMonth(ttlDate.getMonth() + 1);
    return {timestamp, ttl: Math.floor(ttlDate.valueOf() / 1000)}; // to seconds
};

const registerVote = async (obj, dyn = docClient) => {
    try {
        const parsedBody = await parseAndValidate(obj);
        if (parsedBody) {
            const dbResult = await dyn.put({
                Item: {
                    ...parsedBody,
                    ...timeParams(),
                },
                ReturnConsumedCapacity: 'TOTAL',
                TableName: VotesTableName,
            }).promise();
            console.log(`saveInDynamo::success! response: ${util.inspect(dbResult, false, null)}`);
            return 'Writing to DB done';
        }
    } catch (ex) {
        return Promise.reject(ex);
    }
};

module.exports = {
    registerVote,
};
