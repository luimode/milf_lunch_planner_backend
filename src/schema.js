const Ajv = require('ajv');

const schemas = [
    {
        $id: 'vote-schema',
        type: 'object',
        properties: {
            voterId: {type: 'string', pattern: "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"},
            votes: {type: 'array', "uniqueItems": true, "minItems": 1, "maxItems:": 10, items: {"type": "string"}}
        },
        removeAdditional: true,
        required: ['votes', 'voterId']
    },
    {
        $id: 'vote-option-schema',
        type: 'object',
        properties: {
            voterId: {type: 'string', pattern: "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"},
            optionName: {type: 'string', pattern: "^[a-zA-Z&-]{3,20}$"},
            url: {type: 'string', pattern: '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'}
        },
        removeAdditional: true,
        required: ['optionName', 'voterId']
    }
];

const ajv = new Ajv({schemas, removeAdditional: true});

const validate = (schemaId, checkedObj) => {
    const check = ajv.getSchema(schemaId);
    return check(checkedObj) ? [] : check.errors.map(e =>
        `${e.message} - ${e.schemaPath}`
    );
};

module.exports = {validate};