const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
    'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
    'Access-Control-Allow-Origin': '*',
};

const okResponse = (body) =>
    ({
        headers,
        statusCode: 200,
        body: JSON.stringify(body),
        isBase64Encoded: false
    });

const errResponse = err => ({headers, statusCode: 400, body: err.message || err});

module.exports = {okResponse, errResponse};
