const AWS = require('aws-sdk');
const VotesTableName = process.env.OPTION || 'LunchPlannerTable';
const docClient = new AWS.DynamoDB.DocumentClient();

const {readOptions} = require('./optionReader');

function isInToday(inputDate) {
    const today = new Date();
    return today.setHours(0, 0, 0, 0) === inputDate.setHours(0, 0, 0, 0);
}

const mapTodayVotes = vote => isInToday(new Date(vote.timestamp)) ? vote : null;

const sortAscending = (vote1, vote2) => (vote1.timestamp - vote2.timestamp);

const collectPerName = (accumulator, vote) => {
    accumulator[vote.name ? vote.name.toLowerCase() : ''] = vote;
    return accumulator;
};

const scanVotes = async (dyn = docClient) => {
    const dbResult = await dyn.scan({
        TableName: VotesTableName,
        ProjectionExpression: 'voter_id, #voterName, votes, #voteTime',
        ExpressionAttributeNames: {
            "#voterName": "name",
            "#voteTime": "timestamp"
        }
    }).promise();
    return dbResult.Items;
};

const mapOptionToVotes = (option, todayVotes) => ({
    name: option.optionName,
    url: option.url || '',
    votes: Object
        .keys(todayVotes)
        .map(voter => todayVotes[voter].votes.includes(option.optionName) && voter)
        .filter(e => e) //cleanup null values
});

const getTodayVotes = async () => {
    const allVotes = await scanVotes();
    return allVotes
        .map(mapTodayVotes)
        .filter(vote => vote) //cleanup null values
        .sort(sortAscending)
        .reduce(collectPerName, {});
};

const readVotes = async () => {
    try {

        const options = await readOptions();
        const todayVotes = getTodayVotes();
        const votes = options.map(option => mapOptionToVotes(option, todayVotes));
        console.log('result votes:' + JSON.stringify(votes, null, 2));
        return votes;

    } catch (ex) {
        console.log('readVotes::error' + ex);
        return Promise.reject(ex);
    }
};

module.exports = {
    readVotes,
};
