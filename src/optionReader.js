const AWS = require('aws-sdk');
const voteOptionsTableName = process.env.LUNCH_OPTIONS_TABLE_NAME || 'LunchOptionsTable';
const docClient = new AWS.DynamoDB.DocumentClient();

const readOptions = async (dyn = docClient) => {
    try {
        const dbResult = await dyn.scan({
            TableName: voteOptionsTableName,
            ProjectionExpression: 'optionName, #URL',
            ExpressionAttributeNames: {
                "#URL": "url",
            }
        }).promise();

        return dbResult.Items;
    } catch (ex) {
        console.log('readOptions::error' + ex);
        return Promise.reject(ex);
    }
};

module.exports = {
    readOptions,
};