## Millennium Falke Lunch Planner - Backend

The backend of a voting chrome extension for the ex-MilFs. Supporting daily lunch since 2019.

### Structure:

##### Infrastructure

* SAM templates defining our lambdas
* API definition

##### Utils

* utility scripts for deployment
* json "event" files for local testing of the lambdas

### Development

   1. make sure you have the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) Installed before running the helper scripts
   2. make sure your [AWS credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) point to the MiLf AWS account (I can provide them)
   3. run `$./prepare.sh` to create a bucket for storing the lambda artifacts (if it doesnt exist yet)
   4. run `$./deploy.sh` to deploy the lambda functions to AWS
