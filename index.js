const {okResponse, errResponse} = require('./src/responses');
const {readVotes} = require('./src/voteReader');
const {registerVote} = require('./src/voteRegister');
const {registerOption} = require('./src/optionRegister');

const postVoteHandler = async(event, context, callback) => {

    try {
        const savedRes = await registerVote(event.body);
        callback(null, okResponse({"message": "vote has been registered"}));

    } catch (err) {
        console.log('Handler::postVoteHandler::terminated with error:' + err);
        callback(null, errResponse(err));
    }
};

const postVoteOptionHandler = async(event, context, callback) => {

    try {
        const savedRes = await registerOption(event.body);
        callback(null, okResponse({"message": "vote option has been added"}));

    } catch (err) {
        console.log('Handler::postVoteOptionHandler::terminated with error:' + err);
        callback(null, errResponse(err));
    }
};

const getVotesHandler = async(event, context, callback) => {
    try {
        const votes = await readVotes();
        callback(null, okResponse({items: votes}));
    } catch (err) {
        console.log('Handler::getVotesHandler::terminated with error:' + err);
        callback(null, errResponse(err));
    }
};

module.exports = {
    postVoteHandler,
    getVotesHandler,
    postVoteOptionHandler,
};