#!/usr/bin/env bash
set -e

AWS_ACCOUNT=`aws sts get-caller-identity --output text --query 'Account'`
ARTIFACT_BUCKET_NAME=lunch-planner-artifacts-${AWS_ACCOUNT}

aws s3 mb s3://${ARTIFACT_BUCKET_NAME}