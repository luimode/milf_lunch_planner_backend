#!/usr/bin/env bash
set -e

AWS_ACCOUNT=`aws sts get-caller-identity --output text --query 'Account'`

STACK_NAME=milf-lunch-planner
ARTIFACT_BUCKET_NAME=lunch-planner-artifacts-${AWS_ACCOUNT}

echo '-----------------------------------'
echo "Deploying Stack ${STACK_NAME}"
echo '-----------------------------------'
# Convert Sam to CF and push up source artifact
aws cloudformation package \
  --template-file ././../infrastructure/lunch-planner.yml \
  --output-template-file ././../infrastructure/.tmp.milf-lunch-planner.yml \
  --s3-prefix ${STACK_NAME}-src \
  --s3-bucket ${ARTIFACT_BUCKET_NAME}

# Deploy CF template
aws cloudformation deploy \
  --template-file ././../infrastructure/.tmp.milf-lunch-planner.yml \
  --stack-name ${STACK_NAME} \
  --capabilities CAPABILITY_IAM \
